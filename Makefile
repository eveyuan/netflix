CC = g++
CFLAGS = -g -std=c++11 -Wall
SOURCES = *.cpp
HEADERS = *.h

all: svd_mu svdpp_mu blend

svd_mu: svd_mu.cpp 
	$(CC) $(CFLAGS) -o svd_mu svd_mu.cpp 

svdpp_mu: svdpp_mu.cpp 
	$(CC) $(CFLAGS) -o svdpp_mu svdpp_mu.cpp 

blend: blend.cpp
	$(CC) $(CFLAGS) -o blend blend.cpp
	
blend_arma: blend_arma.cpp
	$(CC) $(CFLAGS) -o blend_arma blend_arma.cpp -larmadillo

clean:
	rm -f *.o svd_mu svdpp_mu blend blend_arma

.PHONY: all clean

