#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>

#include <math.h>

using namespace std;

//TODO
#define DEBUG false 

#define TRAINING_FILE "mu/all.dta"
#define INDEX_FILE "mu/all.idx"
#define QUAL_FILE "mu/qual.dta"
#define PREDICTIONS_FILE "pp_predictions_7_001_70.txt"
#define PROBE_FILE "pp_probe_7_001_70.dta"

#define MAX_RATINGS 102416306
#define MAX_MOVIES 17770
//#define MAX_MOVIES 7
#define MAX_USERS 458293
#define MAX_DATE 2243

//#define MAX_FEATURES 64
#define MAX_FEATURES 30 
//#define MIN_IMPROVEMENT 0.0001
#define MIN_IMPROVEMENT 0.001
//#define MIN_EPOC 200
#define MIN_EPOCHS 30 
#define L_RATE 0.001
#define INIT 0.1
#define K 0.015
#define LAMBDA_y 0.015

struct Data {
    unsigned int userID;
    unsigned int movieID;
    int rating;
    unsigned int time;
//    float oldRating;  
};

struct Movie {
    unsigned int numRatings;
    unsigned int ratingSum;
    float ratingAvg;

    float y[MAX_FEATURES];    
};

struct User {
    unsigned int numRatings;
    unsigned int ratingSum;
    float ratingAvg;
    
    //bool unratedMovies[MAX_MOVIES]; // unratedMovies[i] is true if
                                    // user did not rate movie i
    float ySum[MAX_FEATURES];
};

struct ProbeCard {
    unsigned int movieID;
    unsigned int userID;
    int rating;
};

// ===========================
//
//    SVDpp calculator class
//
// ===========================
class SVDppCalculator {
    private:
        unsigned int numRatings;
        unsigned int ratingSum;

        vector<Data> ratings;
        vector<ProbeCard> probeSet;
        Movie movies[MAX_MOVIES];
        User users[MAX_USERS];
        float ratingAvg;

        float movieFeatures[MAX_FEATURES][MAX_MOVIES];
        float userFeatures[MAX_FEATURES][MAX_USERS];

        float predictFinRatings(int, int);

    public:
        SVDppCalculator(void);
        ~SVDppCalculator(void) { };

        void init();
        void loadData();
        void fillMatrices();
        void calcFeatures();
        void savePredictions();
        void saveProbePredictions();

        void run();
};

//---------------------
// Initialization
//---------------------
SVDppCalculator::SVDppCalculator(void) {    
}

void SVDppCalculator::init() {    
    // Initialize feature matricies
    for (unsigned int c = 0; c < MAX_USERS; c++) {
        User u;
        u.numRatings = 0;
        u.ratingSum = 0;

        users[c] = u;
    }
    for (unsigned int c = 0; c < MAX_MOVIES; c++) {
        Movie m;
        m.numRatings = 0;
        m.ratingSum = 0;

        movies[c] = m;
    }
    for(int r = 0; r < MAX_FEATURES; r++) {
        for(int c = 0; c < MAX_USERS; c++) {
            userFeatures[r][c]= INIT;
            users[c].ySum[r] = 0;
        }
        for (unsigned int c2 = 0; c2 < MAX_MOVIES; c2++) {
            movieFeatures[r][c2] = INIT;
            movies[c2].y[r] = 0;
        }
    }

    if (DEBUG){
    cout << "    init_init_ySum: " ;
    for (int d = 0; d < MAX_FEATURES; d++ ) {
        cout << users[0].ySum[d] << " ";
    }
    cout << endl;
    }
}


//---------------------------
// Loading the training data
//--------------------------
void SVDppCalculator::loadData() {
    numRatings = 0;
    ratingSum = 0;
    ratingAvg = 0;

    string line;
    vector<int> sorting;
    
    // Get the group data for the objects
    // (base, valid, hidden, probe, qual)
    ifstream idxFile;
    idxFile.open(INDEX_FILE);
    cout << "Loading idx file..." << endl;
    if (idxFile.is_open()) {
        while (getline (idxFile, line)) {
            sorting.push_back(atoi(line.c_str()));
        }
        cout << "  idx file Loaded." << endl;
    }
    else cerr << "Cannot load index file" << endl;
    idxFile.close();

    // store data as Data objects in the ratings array
    ifstream dataFile;
    dataFile.open(TRAINING_FILE);
    unsigned int i = 0;
    int group;
    cout << "Loading data file..." << endl;
    if (dataFile.is_open()) {
        while (getline (dataFile, line)){
            group = sorting[i];
            if (group == 1 || group == 2 || group == 3) {
                string buf;
                stringstream ss(line);
                vector<string> tokens;
                while (ss >> buf) {
                    tokens.push_back(buf);
                }

                Data data;
                data.movieID = atoi(tokens[0].c_str());
                data.userID = atoi(tokens[1].c_str());
                data.time = atoi(tokens[2].c_str()); 
                data.rating = atoi(tokens[3].c_str());
                //data.oldRating = 1;
                ratings.push_back(data);
        
                ratingAvg += data.rating;                
                numRatings++;

                // Populate the "probe" set
                //if (group == 4) {
                //    ProbeCard probeCard;
                //    probeCard.userID = atoi(tokens[0].c_str());
                //    probeCard.movieID = atoi(tokens[1].c_str());
                //    probeCard.rating = atoi(tokens[3].c_str());
                //    probeSet.push_back(probeCard);
               // }

                // also fill in movie and user matrices
                movies[ data.movieID ].numRatings++;
                movies[ data.movieID ].ratingSum += data.rating;
                users[ data.userID ].numRatings++;
                users[ data.userID ].ratingSum += data.rating;       
                
            }
            i++;
        }
        cout << "  data file Loaded." << endl;

    }
    else cerr << "Unable to load data file" << endl;
    dataFile.close();
    ratingAvg /= numRatings; // Calc average rating

    cout << "~~~~AVERAGE RATING " << ratingAvg << endl;
    cout << "~~~~NUM RATINGS    " << numRatings << endl;
}

// ----------------------------------------
// FillMatrices
//   Loops through all the ratings to fill in the Users and Movies matrices 
//    for use in training
// ---------------------------------------- 
void SVDppCalculator::fillMatrices(){
    cout << "Filling in matrices..." << endl;

    unsigned int i;
    // calculate averages
    for (i = 0; i < MAX_MOVIES; i++) {
        // "Better Mean" using variance ratio of 25
        movies[i].ratingAvg = (25.0 * ratingAvg + movies[i].ratingSum) / (25 + movies[i].numRatings);
    }
    for (i = 0; i < MAX_USERS; i++) {
        users[i].ratingAvg = (25.0 * ratingAvg + users[i].ratingSum) / (25 + users[i].numRatings);
    }
}

// ----------------------------------------
// CalcFeatures
//    Calculate features via Koren's SVD++
// ----------------------------------------
void SVDppCalculator::calcFeatures(){
    cout << "Calculating features..." << endl;

    float rmse = 2.0;
    float err, rmse_last, p, sq = 2.0;
    float uf, mf;

    // timer for how long the code takes to run    
    clock_t start;
    double duration;
    if (DEBUG) {
        for (int i = 0; i < MAX_MOVIES; i += 1000)
        {
            cout << i <<  " y1 " << movies[i].y[1] << " y2 " << movies[i].y[2] << endl;
        }
    }
    
    float tmp_sum[MAX_FEATURES];
    for(int e = 0; (e < MIN_EPOCHS || rmse <= rmse_last - MIN_IMPROVEMENT); e++ ) {
        start = clock();
        //if (e % 30 == 0) {
        cout << "*****EPOCH: " << e; 
        cout << "  RMSE: " << rmse << " ";
        cout << endl;
        //}

        rmse_last = rmse;
        float norm;
        unsigned int j = 0;
        for (unsigned int i = 0; i < numRatings; i += j) {
            unsigned int uid = ratings[i].userID;
            sq = 0;
            if (users[uid].numRatings < 1) { norm = 0.0; }
            else { norm = 1.0 / sqrt(users[uid].numRatings); }
            // for every movie rated by this user...
            //for (j = 0; ratings[i+j].userID == uid && 
            //        i+j < numRatings; j++) {
            //    // calculate sumY
            //    for(int f = 0; f < MAX_FEATURES; f++) {
            //        tmp_sum[f] = 0.0;
            //    }
            //}
            
            for(int f = 0; f < MAX_FEATURES; f++) {
                tmp_sum[f] = 0.0;
            }

            if (DEBUG) {
                cout << "   init_tmpsum: ";
                for (int n = 0; n < MAX_FEATURES; n++)
                {
                    cout << tmp_sum[n] << " ";
                }
                cout << endl;

                cout << "   init_ySum: ";
                for (int n = 0; n < MAX_FEATURES; n++)
                {
                    cout << users[uid].ySum[n] << " ";
                }
                cout << endl;
            }


            // predict rating and calculate error
            for (j = 0; ratings[i+j].userID == uid &&
                    i+j < numRatings; j++) {
                unsigned int mid = ratings[i+j].movieID;
                p = predictFinRatings(mid, uid);
                err = (1.0 * ratings[i+j].rating - p);
                sq += err * err;
                
                if (j == 0 && DEBUG) {
                    cout << "i+j:" << i+j << " mid:" << mid << " uid:" << uid << " p:" << p << " err:" << err <<  endl;
                    cout << "  mf:" << movieFeatures[1][mid] << " " << movieFeatures[2][mid];
                    cout << "  uf:" << userFeatures[1][uid] << " " << userFeatures[2][uid];
                    cout << endl;
                    cout << "  mAvg:" << movies[mid].ratingAvg << " uAvg:" << users[uid].ratingAvg << endl;
                    cout << "  y1:" << movies[mid].y[1] << " y2:" << movies[mid].y[2] << endl;
                    cout << "  ySum3:" << users[uid].ySum[3] << " ySUm4:" << users[uid].ySum[4];
                    cout << endl;
                    cout << endl;
                }

                // train the features
                for (int f = 0; f < MAX_FEATURES; f++) {        
                    uf = userFeatures[f][uid];
                    mf = movieFeatures[f][mid];
                    userFeatures[f][uid] += (float)(L_RATE * (err * mf - K * uf));
                    movieFeatures[f][mid] += (float)(L_RATE * (err * 
                            (uf + norm * users[uid].ySum[f]) - K * mf));
                    // keep track of the gradient
                    tmp_sum[f] += (err * norm * mf);
                }
            }
            if (DEBUG) {
                cout << "   done_tmpsum: ";
                for (int n = 0; n < MAX_FEATURES; n++)
                {
                    cout << tmp_sum[n] << " ";
                }
                cout << endl;
            }

            // update y values (movie weights)
            for (j = 0; ratings[i+j].userID == uid &&
                    i+j < numRatings; j++) {
                unsigned int mid = ratings[i+j].movieID;
                for (unsigned int f = 0; f < MAX_FEATURES; f++) {
                    float dy = (float)(L_RATE * tmp_sum[f] - LAMBDA_y * movies[mid].y[f]);
                    if (j == 0 && DEBUG)
                    {
                        cout << "   dy:" << dy << endl;
                        cout << "    tmpsum:" << tmp_sum[f] << " my:" << movies[mid].y[f];
                        cout << endl;
                    }
                    movies[mid].y[f] += dy;
                    users[uid].ySum[f] += dy;
                }
            }
        }

        rmse = sqrt(sq/numRatings);
         
        duration = (clock() - start) / (double) CLOCKS_PER_SEC;
        cout << "epoch: " << e << " time: " << duration << endl;
    }
}

//float SVDppCalculator::predictRating(int ratingIdx, int f){
//    int mID = ratings[ratingIdx].movieID;
//    int uID = ratings[ratingIdx].userID;
//
//    float sum = movies[mID].ratingAvg + users[uID].ratingAvg;
//
//    // use the cached rating
//    if (ratings[ratingIdx].oldRating > 0)
//    {
//        sum = ratings[ratingIdx].oldRating;
//    }
//    
//    // do the dot product
//    sum += movieFeatures[f][mID] * userFeatures[f][uID];
//
//    if(sum > 5)
//        sum = 5;
//    if(sum < 1)
//        sum = 1;
//
//    return sum;
//}

// -------------------------
// Predict ratings for all user/movie pairs.
//     Loops through all the features to calculate the final result
// --------------------------
float SVDppCalculator::predictFinRatings(int movieID, int userID) {
    float sum = movies[movieID].ratingAvg + users[userID].ratingAvg;
    for (int f=0; f < MAX_FEATURES; f++) {
        sum += movieFeatures[f][movieID] * (userFeatures[f][userID] + users[userID].ySum[f]);
        if (DEBUG) {
        cout << "----" << sum << endl;
        cout << "------" << movieFeatures[f][movieID] << " ";
        cout << userFeatures[f][userID] << " " << users[userID].ySum[f] << endl;
        }

        if (sum > 5) sum = 5;
        if (sum < 1) sum = 1;
    }
    return sum;
}

void SVDppCalculator::savePredictions(){
    cout << "Saving predictions..." << endl;
    string line; 
    unsigned int quserID, qmovieID;

    ofstream predictionFile;
    predictionFile.open(PREDICTIONS_FILE);

    ifstream qualFile;
    qualFile.open(QUAL_FILE);
    if (qualFile.is_open()) {
        while (getline (qualFile, line)){
            string buf;
            stringstream ss(line);
            vector<string> tokens;
            while (ss >> buf) {
                tokens.push_back(buf);
            }

            quserID = atoi(tokens[0].c_str());
            qmovieID = atoi(tokens[1].c_str());
            
            predictionFile << predictFinRatings(qmovieID, quserID) << endl;    
            
        }         
    }
    qualFile.close();
    predictionFile.close();

}

/**
 * @brief   Saves predictions for the "probe" set
 */
void SVDppCalculator::saveProbePredictions() {

    // Print a status message
    cout << "Saving predictions for the 'probe' set..." << endl;
    
    // Variables
    string line;
    ofstream probeFile;
    
    // Open the output file
    probeFile.open(PROBE_FILE);
    
    // Compute and write: "[predicted rating] [actual rating]"
    for (unsigned int i = 0; i < probeSet.size(); i++) {
        probeFile << predictFinRatings(probeSet[i].movieID, probeSet[i].userID) << " " << probeSet[i].rating << endl;
    }
    
    // Close the output file
    probeFile.close();

}

void SVDppCalculator::run() {
    init();
    loadData();
    fillMatrices();
    calcFeatures();
    savePredictions();
  //  saveProbePredictions(); 
}

double calcRMSE(float *yPred, float *y){
    double rmse = 0;
    unsigned int i;
    for(i = 0; i < MAX_RATINGS; i++){
        rmse += pow((*(y+i) - *(yPred+i)), 2.0);
    }
    rmse /= i;
    return sqrt(rmse);
}

//float *text2array(string filename){
//    float * output = new float[MAX_RATINGS];
//    string line;
//    ifstream myfile;
//    myfile.open(filename);
//    unsigned int i = 0;
//    if(myfile.is_open()){
//        while(getline(myfile,line)){
//            output[i] = stof(line);
//            i++;
//        }
//        myfile.close();
//        return output;
//    }
//    else cout << "text2vector: unable to open file\n";
//    return output;
//}
//////////////////////////////////////////////////
int main(int argc, char ** argv) {
    SVDppCalculator * calculator = new SVDppCalculator();
   
    calculator->run();
    //calculator->init();
    //calculator->loadData();
    //calculator->fillMatrices();
    //calculator->calcFeatures();
    //calculator->savePredictions();
}








