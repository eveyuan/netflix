**************************************
Eve's First Commit Comments (4/13/15):
--------------------------------------

- Working with mu dataset

- Data organization:
	- movieItem: struct that holds movie information (movieNum = movieID,
	rateTime = time of rating, rating = rating [1 - 5])
	
	- usersTraining: vector of vectors of movieItems. Each index of this
	vector is a userID. Each index has a vector of movieItems that
	represent the movies that that user has rated.

- Global vars:
	- Various params for svd algorithm

	- sample average

- Data processing:
	- Filters out data from groups 1, 2, and 3 (training sets) and 
	populates usersTraining
	

- rateMovie:
	- Takes in userID and movieID, calculates a rating based on 
	summing contributions for user features matrix and movie features
	matrix
	
	- Limits answer between 1 and 5


***************************************
Dennis's Notes on Blending (5/16/2015):
---------------------------------------

    Linear-regression algorithm not complete, but should yield okay results
    at the moment
    
    svd_mu should now spit out two files:
        "predictions.txt" that contains the qual-set predictions
        "probe.dta" that contains the probe-set predictions
    
    After running each model, please rename "predictions.txt" and "probe.dta"
    to "predictionsNUMBER.dta" and "probeNUMBER.dta" where NUMBER is a unique 
    integer ID for that model. The final set of ID's should be continuous and 
    consecutive. For example,
    
        predictions1.dta    probe1.dta
        predictions2.dta    probe2.dta
        predictions3.dta    probe3.dta

------------
INSTRUCTIONS
------------

There are currently two ways to blend: by hand (i.e., setting the weights 
yourself), and using linear regression.

BY HAND

    =====
    Steps  
    =====

    1. Set the blending weights to whatever you desire in the 
       manualBlend() function of blend.cpp

    2. At the terminal, run:

        > make blend
        > ./blend

    ===========
    Description
    ===========

    "blend" takes predictions1.dta through predictions[NUM_MODELS].dta
    and computes a single, blended set of predictions based on the 
    values in weights[NUM_MODELS]. These final predictions are stored
    in predictions.dta.

    Go to blend.cpp and find the manualBlend() function. By default, it 
    will initialize each weight to (1.0 / [# of weights]). The weights
    are stored in weights[NUM_MODELS] and you can set them however you 
    want.
    
LINEAR REGRESSION

    =====
    Steps  
    =====
    
    At the terminal, run:

        > make blend_arma
        > ./blend_arma

    ===========
    Description
    ===========

    For this you will need:
        - Armadillo installed on whatever computer you're compiling with
        - Both the prediction and probe files for each model you're
          blending (e.g., "predictions1.dta" and "probe1.dta" should 
          store the "qual" predictions and the "probe" predictions for 
          the first model)
    
    "blend_arma" uses the probe predictions and actual ratings to perform 
    basic linear regression (pseudoinverse method). It will then take the 
    weights computed from this method and blend the "qual" predictions 
    together from into a single, blended set of predictions.

    The input files are predictions1.dta through predictions[NUM_MODELS].dta
    and probe1.dta through probe[NUM_MODELS].dta. The final predictions are
    stored in predictions.dta.
    

