#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define INDEX_FILE  "mu/all.idx"
#define PROBE       4

int main(int argc, char ** argv) {

    int counter;
    string line;
    ifstream idxFile;
    
    cout << "Loading idx file..." << endl;
        
    counter = 0;
    idxFile.open(INDEX_FILE);

    if (idxFile.is_open()) {
        while (getline(idxFile, line)) {
            if (atoi(line.c_str()) == PROBE) {
                counter++;
            }
        }
        cout << "Number of 'probe' ratings: " << counter << endl;
    } else {
        cerr << "Cannot load index file." << endl;
    }
    
    switch(counter) {
        case 1374739:
            cout << "'Probe' data counted successfully!" << endl;
        default:
            cout << "Goodbye!" << endl;
        break;
    }
    
    idxFile.close();
    
}








