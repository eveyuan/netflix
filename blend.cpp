#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ctime>

using namespace std;

#define NUM_MODELS          3
#define NUM_PROBE           1374739
#define NUM_QUAL            2749898
#define PROBE_PREFIX        "probe"
#define QUAL_PREFIX         "predictions"
#define FILE_SUFFIX         "dta"
#define OUTPUT_FILE         "predictions.dta"

/**
 *  @brief  "Blender" class definition.
 */
class Blender {

    private:
        bool probeLoaded;
        bool qualLoaded;
        bool weightsDefined;
        float probeReal[NUM_PROBE];
        float probeGuess[NUM_MODELS][NUM_PROBE];
        float predictions[NUM_MODELS][NUM_QUAL];
        float weights[NUM_MODELS];
    
    public:
        
        Blender();
        ~Blender();
        
        bool readProbeData();
        bool readQualData();
        void printProbeData();
        void printQualData();
        void fillMatrices();

        void manualBlend();
        void calcWeights();
        void calcRMSE();
        float calcBlendedRating(int i);     
        
        void savePredictions();

};

/**
 *  @brief  The constructor for the "Blender" class
 */
Blender::Blender() {
    probeLoaded = false;
    qualLoaded = false;
    weightsDefined = false;
}

/**
 *  @brief  The destructor for the "Blender" class
 */
Blender::~Blender() {
    // Nothing for now...
}

/** TODO Check to see if this works
 *  @brief  Reads the prediction file for the "probe" set of each model and stores the predictions
 */
bool Blender::readProbeData() {

    // Variables for reading the data
    int counter;
    bool error;
    string temp;
    ifstream probeFile;
    
    // Variables for timing the read(s)
    clock_t start;
    double duration;
    
    error = false;
    for (int i = 0; i < NUM_MODELS && !error; i++) {
        
        // Start the clock
        start = clock();
        
        // Open the prediction file
        probeFile.open(PROBE_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX);
        cout << "Reading 'probe' data, file " << (i + 1) << " of " << NUM_MODELS << "..." << endl;

        // Read the data from the prediction file
        if (probeFile.is_open()) {
            
            // Loop through the file, line by line
            switch (i) {
                case 0:
                    for (counter = 0; counter < NUM_PROBE && getline(probeFile, temp); counter++) {
                        probeReal[counter] = atof(temp.substr(temp.find(' ') + 1).c_str());
                        probeGuess[i][counter] = atof(temp.substr(0, temp.find(' ')).c_str());
                    }
                break;
                default:
                    for (counter = 0; counter < NUM_PROBE && getline(probeFile, temp); counter++) {
                        probeGuess[i][counter] = atof(temp.substr(0, temp.find(' ')).c_str());
                    }
                break;
            }
            
            // Did the number of lines match the expected number of predictions?
            if (counter != NUM_PROBE) {
                error = true;
                cerr << "Incorrect quantity of 'probe' data!" << endl;
            } else {
                // If loading was successful, print the elapsed time in seconds
                duration = (clock() - start) / (double)CLOCKS_PER_SEC;
                cout << "Data loaded. Time: " << duration << " seconds" << endl;
            }
            
        } else {
            cerr << "Cannot open file: " << PROBE_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX << endl;
            error = true;
        }
        
        // Close the prediction file
        probeFile.close();
        
    }
    
    // Was all the data loaded successfully?
    if (!error) {
        cout << "All predictions successfully read!" << endl;
        probeLoaded = true;
    } else {
        cout << "Predictions not successfully read!" << endl;
    }
    
    return error;

}

/**
 *  @brief  Reads the prediction file for the "qual" set of each model and stores the predictions
 */
bool Blender::readQualData() {

    // Variables for reading the data
    int counter;
    bool error;
    string temp;
    ifstream predictionFile;
    
    // Variables for timing the read(s)
    clock_t start;
    double duration;
    
    error = false;
    for (int i = 0; i < NUM_MODELS && !error; i++) {
        
        // Start the clock
        start = clock();
        
        // Open the prediction file
        predictionFile.open(QUAL_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX);
        cout << "Reading prediction file " << (i + 1) << " of " << NUM_MODELS << "..." << endl;

        // Read the data from the prediction file
        if (predictionFile.is_open()) {
            
            // Loop through the file, line by line
            for (counter = 0; counter < NUM_QUAL && getline(predictionFile, temp); counter++) {
                predictions[i][counter] = atof(temp.c_str());
            }
            
            // Did the number of lines match the expected number of predictions?
            if (counter != NUM_QUAL) {
                error = true;
                cerr << "Incorrect number of predictions!" << endl;
            } else {
                // If loading was successful, print the elapsed time in seconds
                duration = (clock() - start) / (double)CLOCKS_PER_SEC;
                cout << "Data loaded. Time: " << duration << " seconds" << endl;
            }
            
        } else {
            cerr << "Cannot open file: " << QUAL_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX << endl;
            error = true;
        }
        
        // Close the prediction file
        predictionFile.close();
        
    }
    
    // Was all the data loaded successfully?
    if (!error) {
        cout << "All predictions successfully read!" << endl;
        qualLoaded = true;
    } else {
        cout << "Predictions not successfully read!" << endl;
    }
    
    return error;

}

/**
 *  @brief  Prints the loaded 'probe' data. (Will probably take a very long time.)
 */
void Blender::printProbeData() {
    if (probeLoaded) {
        for (int i = 0; i < NUM_MODELS; i++) {
            cout << "[MODEL_NUMBER - PROBE_NUMBER]: [PREDICTED_VALUE]\t[ACTUAL_VALUE]" << endl;
            for (int j = 0; j < NUM_PROBE; j++) {
                cout << (i + 1) << "-" << (j + 1) << ": " << probeGuess[i][j] << "\t" << probeReal[j] <<  endl;
            }
        }
    } else {
        cerr << "Unable to print 'probe' data; not yet loaded!" << endl;
    }
}

/**
 *  @brief  Prints the loaded prediction data. (Will probably take a very long time.)
 */
void Blender::printQualData() {
    if (qualLoaded) {
        for (int i = 0; i < NUM_MODELS; i++) {
            cout << "[MODEL_NUMBER - PREDICTION_NUMBER]: [PREDICTION_VALUE]" << endl;
            for (int j = 0; j < NUM_QUAL; j++) {
                cout << (i + 1) << "-" << (j + 1) << ": " << predictions[i][j] << endl;
            }
        }
    } else {
        cerr << "Unable to print predictions; data not yet loaded!" << endl;
    }
}

/**
 *  @brief  Set the blending weights by hand (for debugging)
 */
void Blender::manualBlend() {

    // Everyone gets the same weight!
    float evenWeight = 1.0 / (float)NUM_MODELS;
    for (int i = 0; i < NUM_MODELS; i++) {
        weights[i] = evenWeight;
    }

    /*    
    // Manually set the weights for three prediction models
    weights[0] = 0.25;
    weights[1] = 0.50;
    weights[2] = 0.25;
    */

    weightsDefined = true;

}

/**
 *  @brief  Saves the final predictions to the specified file
 */
void Blender::savePredictions() {

    // Declare the output file
    ofstream outputFile;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Computing and writing final predictions...";
    
    // Start the clock
    start = clock();
  
    // Open the output file
    outputFile.open(OUTPUT_FILE);
  
    // Compute each blended rating and write to the output file
    for (int i = 0; i < NUM_QUAL; i++) {
        outputFile << calcBlendedRating(i) << endl;   
    }
    
    // Close the output file
    outputFile.close();
    
    // Display a status message and the elapsed time in seconds
    duration = (clock() - start) / (double)CLOCKS_PER_SEC;
    cout << "complete." << endl << "Time: " << duration << " seconds." << endl;

}

/**
 *  @brief  Computes and returns the predicted rating for the given index
 */
float Blender::calcBlendedRating(int i) {

    float rating = 0.0;

    if (!weightsDefined) {
        cerr << "Cannot compute rating! Weights not yet initialized!" << endl;
    } else {
        if ((i < 0) || (i >= NUM_QUAL)) {
            cerr << "Cannot compute rating for invalid index: " << i << endl;
        } else {
            for (int j = 0; j < NUM_MODELS; j++) {
                rating += weights[j] * predictions[j][i];
            }
        }
    }
    
    return rating;

}

/**
 *  @brief  'main' function. Creates a single Blender and runs it on the data.
 */
int main(void) {

    // Print a friendly message
    cout << "Let's blend!" << endl;

    // Create the Blender object
    Blender *blender = new Blender();

    // Read in the 'probe' data
    //blender->readProbeData();
    
    // DEBUG: Print out the 'probe' data
    //blender->printProbeData();

    // Read in the 'qual' data
    blender->readQualData();
    
    // DEBUG: Perform a crappy blend
    blender->manualBlend();
    
    // Save the blending results
    blender->savePredictions();
    
    // Delete the Blender when finished
    delete blender;

    return 0;   

}
