#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>

#include <math.h>

using namespace std;

#define TRAINING_FILE "mu/all.dta"
#define INDEX_FILE "mu/all.idx"
#define QUAL_FILE "mu/qual.dta"
#define PREDICTIONS_FILE "predictions.txt"
#define PROBE_FILE "probe.dta"

#define MAX_RATINGS 102416306
#define MAX_MOVIES 17770
//#define MAX_MOVIES 7
#define MAX_USERS 458293
#define MAX_DATE 2243

//#define MAX_FEATURES 64
#define MAX_FEATURES 50
#define MIN_IMPROVEMENT 0.0001
//#define MIN_EPOCHS 120
#define MIN_EPOCHS 100
#define L_RATE 0.001
#define INIT 0.1
#define K 0.015

struct Data {
    unsigned int userID;
    unsigned int movieID;
    int rating;
    unsigned int time;
    float oldRating;  
};

struct Movie {
    unsigned int movieID;
    unsigned int numRatings;
    unsigned int ratingSum;
    float ratingAvg;
};

struct User {
    unsigned int userID;
    unsigned int numRatings;
    unsigned int ratingSum;
    float ratingAvg;
};

struct ProbeCard {
    unsigned int movieID;
    unsigned int userID;
    int rating;
};

// ===========================
//
//    SVD calculator class
//
// ===========================
class SVDCalculator {
    private:
        unsigned int numRatings;
        unsigned int ratingSum;
        double ratingAvg;

        vector<Data> ratings;
        vector<ProbeCard> probeSet;
        Movie movies[MAX_MOVIES];
        User users[MAX_USERS];
        float movieFeatures[MAX_FEATURES][MAX_MOVIES];
        float userFeatures[MAX_FEATURES][MAX_USERS];

        float predictRating(int, int);
        float predictFinRatings(int, int);

    public:
        SVDCalculator(void);
        ~SVDCalculator(void) { };

        void loadData();
        void fillMatrices();
        void calcFeatures();
        void savePredictions();
        void saveProbePredictions();
};

//---------------------
// Initialization
//---------------------
SVDCalculator::SVDCalculator(void) {    
    // Initialize feature matricies
    for(unsigned int r = 0; r < MAX_FEATURES; r++) {
        for(int c = 0; c < MAX_USERS; c++) {
            userFeatures[r][c]= INIT;
        }
        for (unsigned int c2 = 0; c2 < MAX_MOVIES; c2++) {
            movieFeatures[r][c2] = INIT;
        }
    }
    for (unsigned int c = 0; c < MAX_USERS; c++) {
        User u;
        u.userID = 0;
        u.numRatings = 0;
        u.ratingSum = 0;
        users[c] = u;
    }
    for (unsigned int c = 0; c < MAX_MOVIES; c++) {
        Movie m;
        m.movieID = 0;
        m.numRatings = 0;
        m.ratingSum = 0;
        movies[c] = m;
    }
}

//---------------------------
// Loading the training data
//--------------------------
void SVDCalculator::loadData() {
    numRatings = 0;
    ratingSum = 0;
    ratingAvg = 0;

    string line;
    vector<int> sorting;
    
    // Get the group data for the objects
    // (base, valid, hidden, probe, qual)
    ifstream idxFile;
    idxFile.open(INDEX_FILE);
    cout << "Loading idx file..." << endl;
    if (idxFile.is_open()) {
        while (getline (idxFile, line)) {
            sorting.push_back(atoi(line.c_str()));
        }
        cout << "  idx file Loaded." << endl;
    }
    else cerr << "Cannot load index file" << endl;
    idxFile.close();

    // store data as Data objects in the ratings array
    ifstream dataFile;
    dataFile.open(TRAINING_FILE);
    unsigned int i = 0;
    int group;
    cout << "Loading data file..." << endl;
    if (dataFile.is_open()) {
        while (getline (dataFile, line)){
            group = sorting[i];
            // If the group is not part of the "qual" set...
            if ((group < 5) && (group > 0)) {
                string buf;
                stringstream ss(line);
                vector<string> tokens;
                while (ss >> buf) {
                    tokens.push_back(buf);
                }

                Data data;
                data.movieID = atoi(tokens[0].c_str());
                data.userID = atoi(tokens[1].c_str());
                data.time = atoi(tokens[2].c_str());
                data.rating = atoi(tokens[3].c_str());
                data.oldRating = 1;
               
                ratings.push_back(data);
                ratingAvg += data.rating;

                // Populate the "probe" set
                if (group == 4) {
                    ProbeCard probeCard;
                    probeCard.userID = atoi(tokens[0].c_str());
                    probeCard.movieID = atoi(tokens[1].c_str());
                    probeCard.rating = atoi(tokens[3].c_str());
                    probeSet.push_back(probeCard);
                }

                numRatings++;

            }
            i++;
        }
        cout << "  data file Loaded." << endl;

    }
    else cerr << "Unable to load data file" << endl;
    dataFile.close();
    ratingAvg /= numRatings; // Calc average rating

    cout << "~~~~AVERAGE RATING " << ratingAvg << endl;
    cout << "~~~~NUM RATINGS    " << numRatings << endl;

}

// ----------------------------------------
// FillMatrices
//   Loops through all the ratings to fill in the Users and Movies matrices 
//    for use in training
// ---------------------------------------- 
void SVDCalculator::fillMatrices(){
    cout << "Filling in matrices..." << endl;

    unsigned int i;
    for (i = 0; i < numRatings; i++) {
        movies[ ratings[i].movieID ].movieID = ratings[i].movieID;
        movies[ ratings[i].movieID ].numRatings++;
        movies[ ratings[i].movieID ].ratingSum += ratings[i].rating;
        
        users[ ratings[i].userID ].userID = ratings[i].userID;
        users[ ratings[i].userID ].numRatings++;
        users[ ratings[i].userID ].ratingSum += ratings[i].rating;       
    } 
    // calculate averages
    for (i = 0; i < MAX_MOVIES; i++) {
        //movies[i].ratingAvg = (1.0 * movies[i].ratingSum) / movies[i].numRatings;
        // "Better Mean" using variance ratio of 25
        movies[i].ratingAvg = (25.0 * ratingAvg + movies[i].ratingSum) / (25 + movies[i].numRatings);
    }
    for (i = 0; i < MAX_USERS; i++) {
        //users[i].ratingAvg = (1.0 * users[i].ratingSum) / users[i].numRatings;
        users[i].ratingAvg = (25.0 * ratingAvg + users[i].ratingSum) / (25 + users[i].numRatings);
    }
}

// ----------------------------------------
// CalcFeatures
//    Use the algorithm described in Simon Funk's paper to train features
// ----------------------------------------
void SVDCalculator::calcFeatures(){
    cout << "Calculating features..." << endl;

    double rmse = 2.0;
    double err, rmse_last, p, sq = 2.0;
    double uf, mf;

    // timer for how long the code takes to run    
    clock_t start;
    double duration;

    unsigned int i;
    for(int f = 0; f < MAX_FEATURES; f++) {
        start = clock();

        for(int e = 0; (e < MIN_EPOCHS || rmse <= rmse_last - MIN_IMPROVEMENT); e++ ) {
            
            // Prints out the RMSE at every 100 epochs
            if (e % 100 == 0) {
                cout << "   epoch: " << e; 
                cout << "  rmse: " << rmse << " ";
                cout << endl;
            }

            sq = 0;
            rmse_last = rmse;
            for (i = 0; i < numRatings; i++) {
                // predict rating and calculate error
                p = predictRating(i, f);
                err = (1.0 * ratings[i].rating - p);
                sq += err * err;
                
                // train the features
                uf = userFeatures[f][ratings[i].userID];
                mf = movieFeatures[f][ratings[i].movieID];
            
                userFeatures[f][ratings[i].userID] += (float)(L_RATE * (err * mf - K * uf));
                movieFeatures[f][ratings[i].movieID] += (float)(L_RATE * (err * uf - K * mf));
            }
            rmse = sqrt(sq/numRatings);

        }
        // Cache off old predictions
        for (i = 0; i < numRatings; i++)
        {
            ratings[i].oldRating = (float)predictRating(i, f);
        }
        duration = (clock() - start) / (double) CLOCKS_PER_SEC;
        cout << "Feature: " << f << " time: " << duration << endl;
    }
}

float SVDCalculator::predictRating(int ratingIdx, int f){
    int mID = ratings[ratingIdx].movieID;
    int uID = ratings[ratingIdx].userID;

    float sum = movies[mID].ratingAvg + users[uID].ratingAvg;

    // use the cached rating
    if (ratings[ratingIdx].oldRating > 0)
    {
        sum = ratings[ratingIdx].oldRating;
    }
    
    // do the dot product
    sum += movieFeatures[f][mID] * userFeatures[f][uID];

    if(sum > 5)
        sum = 5;
    if(sum < 1)
        sum = 1;

    return sum;
}

// -------------------------
// Predict ratings for all user/movie pairs.
//     Loops through all the features to calculate the final result
// --------------------------
float SVDCalculator::predictFinRatings(int movieID, int userID) {
    float sum = 1;

    for (int f=0; f < MAX_FEATURES; f++) {
        sum += movieFeatures[f][movieID] * userFeatures[f][userID];
        if (sum > 5) sum = 5;
        if (sum < 1) sum = 1;
    }

    return sum;
}

void SVDCalculator::savePredictions(){
    cout << "Saving predictions..." << endl;
    string line; 
    unsigned int quserID, qmovieID;

    ofstream predictionFile;
    predictionFile.open(PREDICTIONS_FILE);

    ifstream qualFile;
    qualFile.open(QUAL_FILE);
    if (qualFile.is_open()) {
        while (getline (qualFile, line)){
            string buf;
            stringstream ss(line);
            vector<string> tokens;
            while (ss >> buf) {
                tokens.push_back(buf);
            }

            quserID = atoi(tokens[0].c_str());
            qmovieID = atoi(tokens[1].c_str());
            
            predictionFile << predictFinRatings(qmovieID, quserID) << endl;    
            
        }         
    }
    qualFile.close();
    predictionFile.close();

}

/**
 * @brief   Saves predictions for the "probe" set
 */
void SVDCalculator::saveProbePredictions() {

    // Print a status message
    cout << "Saving predictions for the 'probe' set..." << endl;
    
    // Variables
    string line;
    ofstream probeFile;
    
    // Open the output file
    probeFile.open(PROBE_FILE);
    
    // Compute and write: "[predicted rating] [actual rating]"
    for (unsigned int i = 0; i < probeSet.size(); i++) {
        probeFile << predictFinRatings(probeSet[i].movieID, probeSet[i].userID) << " " << probeSet[i].rating << endl;
    }
    
    // Close the output file
    probeFile.close();

}

double calcRMSE(float *yPred, float *y){
    double rmse = 0;
    unsigned int i;
    for(i = 0; i < MAX_RATINGS; i++){
        rmse += pow((*(y+i) - *(yPred+i)), 2.0);
    }
    rmse /= i;
    return sqrt(rmse);
}

float *text2array(string filename){
    float * output = new float[MAX_RATINGS];
    string line;
    ifstream myfile;
    myfile.open(filename);
    unsigned int i = 0;
    if(myfile.is_open()){
        while(getline(myfile,line)){
            output[i] = stof(line);
            i++;
        }
        myfile.close();
        return output;
    }
    else cout << "text2vector: unable to open file\n";
    return output;
}
//////////////////////////////////////////////////
int main(int argc, char ** argv) {
    SVDCalculator * calculator = new SVDCalculator();

    calculator->loadData();
    calculator->fillMatrices();
    calculator->calcFeatures();
    calculator->savePredictions();
    calculator->saveProbePredictions();
}








