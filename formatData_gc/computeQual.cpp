#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <limits>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define QUAL_FILE       "qual.dta"
#define U_FILE          "train_gc.dta_U.mm"
#define V_FILE          "train_gc.dta_V.mm"
#define UBIAS_FILE      "train_gc.dta_U_bias.mm"
#define VBIAS_FILE      "train_gc.dta_V_bias.mm"
#define MEAN_FILE       "train_gc.dta_global_mean.mm"
#define OUTPUT_FILE     "predictions_pp.dta"
#define NUM_QUAL        2749898
#define HEADER_SIZE     3
#define NUM_FACTORS     20

/**
 *  @brief  http://stackoverflow.com/questions/5207550/in-c-is-there-a-way-to-go-to-a-specific-line-in-a-text-file
 */
std::fstream& goToLine(std::fstream& file, unsigned int num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return file;
}

/**
 *  @brief  
 */
void generatePredictions() {
    
    int counter;
    
    int userID;
    int movieID;
    
    double mean;
    double u_bias;
    double v_bias;
    double rating;
    
    double u_factors[NUM_FACTORS];
    double u_weights[NUM_FACTORS];
    double v_factors[NUM_FACTORS];
    
    // Input and output files
    ifstream qualFile;
    fstream uFile;
    fstream vFile;
    fstream uBiasFile;
    fstream vBiasFile;
    ifstream meanFile;
    ofstream outputFile;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Calculating predictions..." << endl;
    
    // Start the clock
    start = clock();
  
    // Get the global mean
    string meanLine;
    meanFile.open(MEAN_FILE);
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(meanFile, meanLine);
    }
    getline(meanFile, meanLine);
    mean = stod(meanLine);
    cout << "Global mean: " << mean << endl;

    // Open the input and output files for computing and writing the predictions
    qualFile.open(QUAL_FILE);
    uFile.open(U_FILE);
    vFile.open(V_FILE);
    uBiasFile.open(UBIAS_FILE);
    vBiasFile.open(VBIAS_FILE);
    outputFile.open(OUTPUT_FILE);
  
    /* Generate the prediction file! */

    // String for storage
    string qualLine;
        
    counter = 0;
    while (getline(qualFile, qualLine)) {

        // Parse the line from QUAL_FILE
        stringstream qss(qualLine);
        vector<string> qTokens;
        string qBuffer;
        while (qss >> qBuffer) {
            qTokens.push_back(qBuffer);
        }
        
        // Load the "qual" data
        userID = atoi(qTokens[0].c_str());
        movieID = atoi(qTokens[1].c_str());
        
        // Set bias_u (movie)
        string uBiasLine;
        goToLine(uBiasFile, movieID + HEADER_SIZE);
        uBiasFile >> uBiasLine;
        u_bias = stod(uBiasLine);
        //cout << u_bias << endl; // DEBUG
        
        // Set bias_v (user)
        string vBiasLine;
        goToLine(vBiasFile, userID + HEADER_SIZE);
        vBiasFile >> vBiasLine;
        v_bias = stod(vBiasLine);
        //cout << v_bias << endl; // DEBUG
        
        /* Parse the factors and weights from the 'U' (movie) matrix */
        
        string uLine;
        goToLine(uFile, movieID + HEADER_SIZE);
        getline(uFile, uLine);
        //cout << uLine << endl; // DEBUG
        
        stringstream uss(uLine);
        vector<string> uTokens;
        string uBuffer;
        while (uss >> uBuffer) {
            uTokens.push_back(uBuffer);
        }
        //cout << uTokens.size() << endl; // DEBUG
        
        for (int i = 0; i < NUM_FACTORS; i++) {
            u_factors[i] = stod(uTokens[i]);
            //cout << u_factors[i] << endl; // DEBUG
        }
        for (int i = 0; i < NUM_FACTORS; i++) {
            u_weights[i] = stod(uTokens[i + NUM_FACTORS]);
            //cout << u_weights[i] << endl; // DEBUG
        }
        
        /* Parse the factors and weights from the 'V' (user) matrix */

        string vLine;
        goToLine(vFile, userID + HEADER_SIZE);
        getline(vFile, vLine);
        
        stringstream vss(vLine);
        vector<string> vTokens;
        string vBuffer;
        while (vss >> vBuffer) {
            vTokens.push_back(vBuffer);
        }
        
        for (int i = 0; i < NUM_FACTORS; i++) {
            v_factors[i] = stod(vTokens[i]);
            //cout << v_factors[i] << endl; // DEBUG
        }
        
        // Compute the rating (https://groups.google.com/forum/#!topic/graphlab-kdd/4lECy2Td7pI)
        rating = mean + u_bias + v_bias;
        for (int i = 0; i < NUM_FACTORS; i++) {
            rating += v_factors[i] * (u_factors[i] + u_weights[i]);
        }
        
        // Write the rating to the output file
        outputFile << rating << endl;
        
        // Increment the counter
        counter++;
        
    }
    
    // Display a status message and the elapsed time in seconds
    duration = (clock() - start) / (double)CLOCKS_PER_SEC;
    cout << "Complete! Time: " << duration << " seconds." << endl;
    
    // Is the final number of "qual" points correct?
    if (counter != NUM_QUAL) {
        cout << "Counting error!" << endl;
    }
    
    // Close the files
    qualFile.close();
    uFile.close();
    vFile.close();
    uBiasFile.close();
    vBiasFile.close();
    outputFile.close();

}

int main(int argc, char ** argv) {

    // Format the data!
    generatePredictions();
    
}








