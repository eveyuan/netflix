#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <limits>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define QUAL_FILE       "qual.dta"
#define U_FILE          "train_gc.dta_U.mm"
#define V_FILE          "train_gc.dta_V.mm"
#define UBIAS_FILE      "train_gc.dta_U_bias.mm"
#define VBIAS_FILE      "train_gc.dta_V_bias.mm"
#define MEAN_FILE       "train_gc.dta_global_mean.mm"
#define OUTPUT_FILE     "predictions2.dta"
#define NUM_MOVIES      17770
#define NUM_USERS       458293
#define NUM_QUAL        2749898
#define HEADER_SIZE     3
#define NUM_FACTORS     50

// Global storage variables
double mean;
double u_bias[NUM_MOVIES];
double v_bias[NUM_USERS];
double u_factors[NUM_MOVIES][NUM_FACTORS];
double u_weights[NUM_MOVIES][NUM_FACTORS];
double v_factors[NUM_USERS][NUM_FACTORS];

/**
 *  @brief  http://stackoverflow.com/questions/5207550/in-c-is-there-a-way-to-go-to-a-specific-line-in-a-text-file
 */
std::fstream& goToLine(std::fstream& file, unsigned int num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return file;
}

/**
 *  @brief  
 */
void loadData() {

    int counter;

    // Input and output files
    ifstream uFile;
    ifstream vFile;
    ifstream uBiasFile;
    ifstream vBiasFile;
    ifstream meanFile;

    // Print a status message
    cout << "Loading data..." << endl;
    
    // Get the global mean
    string meanLine;
    meanFile.open(MEAN_FILE);
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(meanFile, meanLine);
    }
    getline(meanFile, meanLine);
    mean = stod(meanLine);
    cout << "Global mean: " << mean << endl;
    
    // Open the input and output files
    uFile.open(U_FILE);
    vFile.open(V_FILE);
    uBiasFile.open(UBIAS_FILE);
    vBiasFile.open(VBIAS_FILE);

    // load u_bias
    string uBiasLine;
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(uBiasFile, uBiasLine);
    }
    counter = 0;
    for (int i = 0; i < NUM_MOVIES; i++) {
        getline(uBiasFile, uBiasLine);
        u_bias[i] = stod(uBiasLine);
        counter++;
    }
    if (counter != NUM_MOVIES) {
        cout << "Counting error when loading movie biases!" << endl;
        cout << "Number of movie-biases loaded: " << counter << endl;
    }

    // load v_bias
    string vBiasLine;
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(vBiasFile, vBiasLine);
    }
    counter = 0;
    for (int i = 0; i < NUM_USERS; i++) {
        getline(vBiasFile, vBiasLine);
        v_bias[i] = stod(vBiasLine);
        counter++;
    }
    if (counter != NUM_USERS) {
        cout << "Counting error when loading user biases!" << endl;
        cout << "Number of user-biases loaded: " << counter << endl;
    }

    /* load u_factors and u_weights */
    
    string uLine;
    
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(uFile, uLine);
    }
    
    for (int i = 0; i < NUM_MOVIES; i++) {
    
        getline(uFile, uLine);
        
        stringstream uss(uLine);
        vector<string> uTokens;
        string uBuffer;
        while (uss >> uBuffer) {
            uTokens.push_back(uBuffer);
        }
        
        for (int j = 0; j < NUM_FACTORS; j++) {
            u_factors[i][j] = stod(uTokens[j]);
        }
        for (int j = 0; j < NUM_FACTORS; j++) {
            u_weights[i][j] = stod(uTokens[j + NUM_FACTORS]);
        }
        
    }

    /* load v_factors */

    string vLine;
    
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(vFile, vLine);
    }
    
    for (int i = 0; i < NUM_USERS; i++) {
    
        getline(vFile, vLine);
        
        stringstream uss(vLine);
        vector<string> vTokens;
        string vBuffer;
        while (uss >> vBuffer) {
            vTokens.push_back(vBuffer);
        }
        
        for (int j = 0; j < NUM_FACTORS; j++) {
            v_factors[i][j] = stod(vTokens[j]);
        }
        
    }

    // Print a status message
    cout << "Data loaded!" << endl;

    // Close the files
    uFile.close();
    vFile.close();
    uBiasFile.close();
    vBiasFile.close();

}

/**
 *  @brief  
 */
void generatePredictions() {
    
    int counter;
    
    int userID;
    int movieID;

    double rating;
    
    // Input and output files
    ifstream qualFile;
    ofstream outputFile;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Calculating predictions..." << endl;
    
    // Start the clock
    start = clock();

    // Open the "qual" and output files
    qualFile.open(QUAL_FILE);
    outputFile.open(OUTPUT_FILE);
  
    /* Generate the prediction file! */

    // String for storage
    string qualLine;
        
    counter = 0;
    while (getline(qualFile, qualLine)) {

        // Parse the line from QUAL_FILE
        stringstream qss(qualLine);
        vector<string> qTokens;
        string qBuffer;
        while (qss >> qBuffer) {
            qTokens.push_back(qBuffer);
        }
        
        // Load the "qual" data
        userID = atoi(qTokens[0].c_str());
        movieID = atoi(qTokens[1].c_str());
        
        // Compute the rating (https://groups.google.com/forum/#!topic/graphlab-kdd/4lECy2Td7pI)
        rating = mean + u_bias[movieID - 1] + v_bias[userID - 1];
        for (int i = 0; i < NUM_FACTORS; i++) {
            rating += v_factors[userID - 1][i] * (u_factors[movieID - 1][i] + u_weights[movieID - 1][i]);
        }
        
        // Write the rating to the output file
        outputFile << rating << endl;
        
        // Increment the counter
        counter++;
        
    }
    
    // Display a status message and the elapsed time in seconds
    duration = (clock() - start) / (double)CLOCKS_PER_SEC;
    cout << "Complete! Time: " << duration << " seconds." << endl;
    
    // Is the final number of "qual" points correct?
    if (counter != NUM_QUAL) {
        cout << "Counting error!" << endl;
    }
    
    // Close the files
    qualFile.close();
    outputFile.close();

}

int main(int argc, char ** argv) {

    // Load the data!
    loadData();

    // Format the data!
    generatePredictions();
    
}








