#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define NUM_USERS       458293
#define NUM_MOVIES      17770
#define NUM_RATINGS     102416306
#define NUM_QUAL        2749898
#define NUM_PROBE       1374739
#define PROBE_INDEX     4
#define QUAL_INDEX      5
#define DATA_FILE       "all.dta"
#define INDEX_FILE      "all.idx"
#define OUTPUT_DATA     "train_gc.dta"
#define OUTPUT_PROBE    "probe_gc.dta"

struct Data {
    unsigned int userID;
    unsigned int movieID;
    int rating; 
};

/**
 *  @brief  Removes the "qual" and "probe" datapoints from DATA_FILE and writes the latter to OUTPUT_QUAL. The new DATA_FILE with the "qual" and "probe" points removed is saved to OUTPUT_DATA.
 */
bool formatData() {
    
    // Indicator variables
    bool error;
    unsigned int index;
    unsigned int probeCounter;
    unsigned int qualCounter;
    unsigned long int dataCounter;
    
    // Data-storage variable
    Data dataCard;
    
    // Input and output files
    ifstream dataInputFile;
    ifstream dataIndexFile;
    ofstream dataOutputFile;
    ofstream probeOutputFile;

    // Strings for reading and writing the data
    string inputLine;
    string indexLine;
    string buffer;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Formatting data..." << endl;
    
    // Start the clock
    start = clock();
  
    // Open the input and output files
    dataInputFile.open(DATA_FILE);
    dataIndexFile.open(INDEX_FILE);
    dataOutputFile.open(OUTPUT_DATA);
    probeOutputFile.open(OUTPUT_PROBE);
  
    /* Read from the old files and write to the new files! */
    
    error = false;
    probeCounter = 0;
    qualCounter = 0;
    dataCounter = 0;
    
    if (dataInputFile.is_open() && dataIndexFile.is_open() && dataOutputFile.is_open() && probeOutputFile.is_open()) {

        // Write the header of the "probe" file
        probeOutputFile << "\%\%MatrixMarket matrix coordinate real general" << endl;
        probeOutputFile << "\% Generated DATE" << endl;
        probeOutputFile << NUM_MOVIES << " " << NUM_USERS << " " << NUM_PROBE << endl;
        
        // Write the header of the "train" file
        dataOutputFile << "\%\%MatrixMarket matrix coordinate real general" << endl;
        dataOutputFile << "\% Generated DATE" << endl;
        dataOutputFile << NUM_MOVIES << " " << NUM_USERS << " " << NUM_RATINGS - (NUM_PROBE + NUM_QUAL) << endl;
        
        while (getline(dataInputFile, inputLine) && getline(dataIndexFile, indexLine)) {
        
            // Get the current datapoint's "group"
            index = atoi(indexLine.c_str());

            // Parse the current datapoint
            stringstream ss(inputLine);
            vector<string> tokens;
            while (ss >> buffer) {
                tokens.push_back(buffer);
            }
            
            // Load the current datapoint;
            dataCard.userID = atoi(tokens[0].c_str());
            dataCard.movieID = atoi(tokens[1].c_str());
            dataCard.rating = atoi(tokens[3].c_str());
            
            // Check if the current datapoint is part of the 'qual' or 'probe' sets
            switch (index) {
                case PROBE_INDEX:
                    probeOutputFile << dataCard.movieID << " " << dataCard.userID << " " << dataCard.rating << endl;
                    probeCounter++;
                break;
                case QUAL_INDEX:
                    qualCounter++;
                break;
                default:
                    dataOutputFile << dataCard.movieID << " " << dataCard.userID << " " << dataCard.rating << endl;
                    dataCounter++;
                break;
            }
            
        }
        
        // Is the alleged number of "qual" and "probe" points correct?
        if ((probeCounter != NUM_PROBE) || (qualCounter != NUM_QUAL) || (dataCounter != NUM_RATINGS - (NUM_PROBE + NUM_QUAL))) {
            cout << "Counting error!" << endl;
            error = true;
        }

        // Display a status message and the elapsed time in seconds
        duration = (clock() - start) / (double)CLOCKS_PER_SEC;
        cout << "...complete. Time: " << duration << " seconds." << endl;

    } else {
        cerr << "File I/O error!" << endl;
        error = true;
    }
    
    // Close the files
    dataInputFile.close();
    dataIndexFile.close();
    dataOutputFile.close();
    probeOutputFile.close();
    
    // Was all the data formatted successfully?
    if (!error) {
        cout << "Data successfully formatted!" << endl;
    } else {
        cout << "Data not successfully formatted!" << endl;
    }
    
    return !error;

}

int main(int argc, char ** argv) {

    // Format the data!
    formatData();
    
}








