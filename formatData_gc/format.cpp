#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define NUM_ALL         102416306
#define NUM_QUAL        2749898
#define NUM_PROBE       1374739
#define PROBE_INDEX     4
#define QUAL_INDEX      5
#define DATA_FILE       "all.dta"
#define INDEX_FILE      "all.idx"
#define OUTPUT_DATA     "noTestSet.dta"
#define OUTPUT_PROBE    "probePoints.dta"

/**
 *  @brief  Removes the "qual" and "probe" datapoints from DATA_FILE and writes the latter to OUTPUT_PROBE. The new DATA_FILE with the "qual" and "probe" points removed is saved to OUTPUT_DATA.
 */
bool formatData() {
    
    bool error;
    int index;
    int counterProbe;
    int counterQual;
    int counterAll;
    
    // Input and output files
    ifstream dataInputFile;
    ifstream dataIndexFile;
    ofstream dataOutputFile;
    ofstream probeOutputFile;

    // Strings for reading and writing the data
    string inputLine;
    string indexLine;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Formatting data..." << endl;
    
    // Start the clock
    start = clock();
  
    // Open the input and output files
    dataInputFile.open(DATA_FILE);
    dataIndexFile.open(INDEX_FILE);
    dataOutputFile.open(OUTPUT_DATA);
    probeOutputFile.open(OUTPUT_PROBE);
  
    /* Read from the old files and write to the new files! */
    
    error = false;
    counterProbe = 0;
    counterQual = 0;
    counterAll = 0;
    
    if (dataInputFile.is_open() && dataIndexFile.is_open() && dataOutputFile.is_open() && probeOutputFile.is_open()) {

        while (getline(dataInputFile, inputLine) && getline(dataIndexFile, indexLine)) {

            // Check if the current datapoint is part of the 'qual' or 'probe' sets
            index = atoi(indexLine.c_str());
            switch (index) {
                case PROBE_INDEX:
                    probeOutputFile << inputLine << endl;
                    counterProbe++;
                break;
                case QUAL_INDEX:
                    counterQual++;
                break;
                default:
                    dataOutputFile << inputLine << endl;
                    counterAll++;
                break;
            }
            
        }
        
        // Is the number of points correct?
        if ((counterQual != NUM_QUAL) || (counterProbe != NUM_PROBE) || (counterAll != NUM_ALL - (NUM_QUAL + NUM_PROBE))) {
            cout << "Counting error!" << endl;
            error = true;
        }

        // Display a status message and the elapsed time in seconds
        duration = (clock() - start) / (double)CLOCKS_PER_SEC;
        cout << "...complete. Time: " << duration << " seconds." << endl;

    } else {
        cerr << "File I/O error!" << endl;
        error = true;
    }
    
    // Close the files
    dataInputFile.close();
    dataIndexFile.close();
    dataOutputFile.close();
    probeOutputFile.close();
    
    // Was all the data formatted successfully?
    if (!error) {
        cout << "Data successfully formatted!" << endl;
    } else {
        cout << "Data not successfully formatted!" << endl;
    }
    
    return !error;

}

int main(int argc, char ** argv) {

    // Format the data!
    formatData();
    
}








