#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <limits>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define QUAL_FILE       "qual.dta"
#define U_FILE          "train_gc.dta_U.mm"
#define V_FILE          "train_gc.dta_V.mm"
#define OUTPUT_FILE     "predictions2.dta"
#define NUM_MOVIES      17770
#define NUM_USERS       458293
#define NUM_QUAL        2749898
#define HEADER_SIZE     3
#define NUM_FACTORS     20

// Global storage variables
double mean;
double u_factors[NUM_MOVIES][NUM_FACTORS];
double u_weights[NUM_MOVIES][NUM_FACTORS];
double v_factors[NUM_USERS][NUM_FACTORS];

/**
 *  @brief  http://stackoverflow.com/questions/5207550/in-c-is-there-a-way-to-go-to-a-specific-line-in-a-text-file
 */
std::fstream& goToLine(std::fstream& file, unsigned int num){
    file.seekg(std::ios::beg);
    for(int i=0; i < num - 1; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return file;
}

/**
 *  @brief  
 */
void loadData() {

    int counter;

    // Input and output files
    ifstream uFile;
    ifstream vFile;
    ifstream meanFile;

    // Print a status message
    cout << "Loading data..." << endl;
    
    // Open the input and output files
    uFile.open(U_FILE);
    vFile.open(V_FILE);

    /* load u_factors */
    
    string uLine;
    
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(uFile, uLine);
    }
    
    for (int i = 0; i < NUM_MOVIES; i++) {
    
        getline(uFile, uLine);
        
        stringstream uss(uLine);
        vector<string> uTokens;
        string uBuffer;
        while (uss >> uBuffer) {
            uTokens.push_back(uBuffer);
        }
        
        for (int j = 0; j < NUM_FACTORS; j++) {
            u_factors[i][j] = stod(uTokens[j]);
        }
        
    }

    /* load v_factors */

    string vLine;
    
    for (int i = 0; i < HEADER_SIZE; i++) {
        getline(vFile, vLine);
    }
    
    for (int i = 0; i < NUM_USERS; i++) {
    
        getline(vFile, vLine);
        
        stringstream uss(vLine);
        vector<string> vTokens;
        string vBuffer;
        while (uss >> vBuffer) {
            vTokens.push_back(vBuffer);
        }
        
        for (int j = 0; j < NUM_FACTORS; j++) {
            v_factors[i][j] = stod(vTokens[j]);
        }
        
    }

    // Print a status message
    cout << "Data loaded!" << endl;

    // Close the files
    uFile.close();
    vFile.close();

}

/**
 *  @brief  
 */
void generatePredictions() {
    
    int counter;
    
    int userID;
    int movieID;

    double rating;
    
    // Input and output files
    ifstream qualFile;
    ofstream outputFile;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Calculating predictions..." << endl;
    
    // Start the clock
    start = clock();

    // Open the "qual" and output files
    qualFile.open(QUAL_FILE);
    outputFile.open(OUTPUT_FILE);
  
    /* Generate the prediction file! */

    // String for storage
    string qualLine;
        
    counter = 0;
    while (getline(qualFile, qualLine)) {

        // Parse the line from QUAL_FILE
        stringstream qss(qualLine);
        vector<string> qTokens;
        string qBuffer;
        while (qss >> qBuffer) {
            qTokens.push_back(qBuffer);
        }
        
        // Load the "qual" data
        userID = atoi(qTokens[0].c_str());
        movieID = atoi(qTokens[1].c_str());
        
        // Compute the rating (https://groups.google.com/forum/#!topic/graphlab-kdd/4lECy2Td7pI)
        rating = 0;
        for (int i = 0; i < NUM_FACTORS; i++) {
            rating += v_factors[userID - 1][i] * u_factors[movieID - 1][i];
        }
        
        // Write the rating to the output file
        outputFile << rating << endl;
        
        // Increment the counter
        counter++;
        
    }
    
    // Display a status message and the elapsed time in seconds
    duration = (clock() - start) / (double)CLOCKS_PER_SEC;
    cout << "Complete! Time: " << duration << " seconds." << endl;
    
    // Is the final number of "qual" points correct?
    if (counter != NUM_QUAL) {
        cout << "Counting error!" << endl;
    }
    
    // Close the files
    qualFile.close();
    outputFile.close();

}

int main(int argc, char ** argv) {

    // Load the data!
    loadData();

    // Format the data!
    generatePredictions();
    
}








