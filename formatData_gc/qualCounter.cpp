#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <ctime>
#include <math.h>

using namespace std;

#define QUAL_FILE       "qual.dta"
#define UNRATED_FILE    "qualUnratedMoves.dta"

int main(int argc, char ** argv) {

    int counterIndex;
    int movieIndex;
    int difference;
    
    string line;
    ifstream qualFile;
    vector<int> unrated;
    
    cout << "Loading 'qual' file..." << endl;
        
    counterIndex = 1;
    qualFile.open(QUAL_FILE);

    if (qualFile.is_open()) {
    
        while (getline(qualFile, line)) {

            stringstream ss(line);
            string buffer;
            vector<string> tokens;
            while (ss >> buffer) {
                tokens.push_back(buffer);
            }
            movieIndex = atoi(tokens[1].c_str());
        
            difference = movieIndex - counterIndex;
            while (difference > 0) {
                counterIndex++;
                difference--;
                if (difference > 0) {
                    unrated.push_back(counterIndex);
                }
            }
            
        }
        
        qualFile.close();
        
    } else {
        cerr << "Cannot load 'qual' file." << endl;
    }

    ofstream outFile;
    outFile.open(UNRATED_FILE);
    
    cout << "Unrated movies: " << endl;
    for (unsigned int i = 0; i < unrated.size(); i++) {
        cout << "\t" << unrated[i] << endl;
        outFile << unrated[i] << endl;
    }
    
    outFile.close();

}








