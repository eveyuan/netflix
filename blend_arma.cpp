#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <ctime>
#include <armadillo>

using namespace std;
using namespace arma;

#define MAX_EPOCHS          1                   // Should be '1' for now until multi-epoch training is fixed
#define NUM_MODELS          2
#define NUM_PROBE           1374739
#define NUM_QUAL            2749898
#define PROBE_PREFIX        "probe"
#define QUAL_PREFIX         "predictions"
#define FILE_SUFFIX         "dta"
#define OUTPUT_FILE         "predictions.dta"

/**
 *  @brief  "Blender" class definition.
 */
class Blender {

    private:
    
        bool probeLoaded;
        bool qualLoaded;
        bool finalWeightsDefined;
        bool manualWeightsDefined;
        
        float probeReal[NUM_PROBE];
        float probeGuess[NUM_MODELS][NUM_PROBE];
        float predictions[NUM_MODELS][NUM_QUAL];
        float weights[NUM_MODELS];
        
        fmat X;
        fmat w;
    
    public:
        
        Blender();
        ~Blender();
        
        bool readProbeData();
        bool readQualData();
        void printProbeData();
        void printQualData();
        void fillMatrices();

        void manualBlend();
        void calcWeights();
        void calcRMSE();
        float calcManualRating(int i);     
        
        void writeBlendedPredictions();
        void writeManualPredictions();

};

/**
 *  @brief  The constructor for the "Blender" class
 */
Blender::Blender() {
    probeLoaded = false;
    qualLoaded = false;
    finalWeightsDefined = false;
    manualWeightsDefined = false;
}

/**
 *  @brief  The destructor for the "Blender" class
 */
Blender::~Blender() {
    // Nothing for now...
}

/**
 *  @brief  Calculates the blending weights using linear regression. (Toscher et al. 2009)
 */
void Blender::calcWeights() {

    double errorBest;
    double errorEpoch;

    fmat finalWeights;
    fmat probeRatings;
    fmat probePredict;

    // Initialize X
    fillMatrices();

    // Initialize the weights
    float avgWeight = 1.0 / (double)(NUM_MODELS + 1);
    for (int i = 0; i < NUM_MODELS + 1; i++) {
        w(i, 0) = avgWeight;
    }
    
    // Store the true "probe" ratings in a matrix
    probeRatings = fmat(NUM_PROBE, 1);
    for (int i = 0; i < NUM_PROBE; i++) {
        probeRatings(i, 0) = probeReal[i];
    }

    /* Compute the weights using linear regression... */
    
    // Initialize variables
    finalWeights = fmat(NUM_MODELS + 1, 1);
    errorBest = 9000.1;
    errorEpoch = 1000.0;
    
    // Train each epoch
    for (int numEpoch = 0; numEpoch < MAX_EPOCHS; numEpoch++) {
    
        // DEBUG
        // cout << "\nLOOP: " << numEpoch << endl << "WEIGHTS: " << endl;
        // w.print();

        w = pinv(X) * probeRatings;
        probePredict = X * w;
        
        // TODO: Compute the RMSE

        // If the current weights are better than the previous ones, save them
        if (errorEpoch <= errorBest) {
            errorBest = errorEpoch;
            for (int i = 0; i < NUM_MODELS + 1; i++) {
                finalWeights(i, 0) = w(i, 0);
            }
        }
        
        // TODO DEBUG: Combine the latest predictions with the previous predictions
        // X.insert_cols(4, probePredict);
    
    }

    // Save the final weights
    for (int i = 0; i < NUM_MODELS + 1; i++) {
        w(i, 0) = finalWeights(i, 0);
        finalWeightsDefined = true;
    }

}

/**
 *  @brief  Fills the matrices used by the linear-regression algorithm
 */
void Blender::fillMatrices() {

    // Set the size of the matrices
    X = fmat(NUM_PROBE, NUM_MODELS + 1);
    w = fmat(NUM_MODELS + 1, 1);
    
    // Fill the first column of X with all 1's
    for (int i = 0; i < NUM_PROBE; i++) {
        X(i, 0) = 1.0;
    }

    // Fill in the rest of the columns of X
    for (int i = 0; i < NUM_MODELS; i++) {
        for (int j = 0; j < NUM_PROBE; j++) {
            X(j, i + 1) = probeGuess[i][j];
        }
    }
    
    cout << "'Probe' data loaded into Armadillo matrix." << endl;

}

/**
 *  @brief  Reads the prediction file for the "probe" set of each model and stores the predictions
 */
bool Blender::readProbeData() {

    // Variables for reading the data
    int counter;
    bool error;
    string temp;
    ifstream probeFile;
    
    // Variables for timing the read(s)
    clock_t start;
    double duration;
    
    error = false;
    for (int i = 0; i < NUM_MODELS && !error; i++) {
        
        // Start the clock
        start = clock();
        
        // Open the prediction file
        probeFile.open(PROBE_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX);
        cout << "Reading 'probe' data, file " << (i + 1) << " of " << NUM_MODELS << "..." << endl;

        // Read the data from the prediction file
        if (probeFile.is_open()) {
            
            // Loop through the file, line by line
            switch (i) {
                case 0:
                    for (counter = 0; counter < NUM_PROBE && getline(probeFile, temp); counter++) {
                        probeReal[counter] = atof(temp.substr(temp.find(' ') + 1).c_str());
                        probeGuess[i][counter] = atof(temp.substr(0, temp.find(' ')).c_str());
                    }
                break;
                default:
                    for (counter = 0; counter < NUM_PROBE && getline(probeFile, temp); counter++) {
                        probeGuess[i][counter] = atof(temp.substr(0, temp.find(' ')).c_str());
                    }
                break;
            }
            
            // Did the number of lines match the expected number of predictions?
            if (counter != NUM_PROBE) {
                error = true;
                cerr << "Incorrect quantity of 'probe' data!" << endl;
            } else {
                // If loading was successful, print the elapsed time in seconds
                duration = (clock() - start) / (double)CLOCKS_PER_SEC;
                cout << "Data loaded. Time: " << duration << " seconds" << endl;
            }
            
        } else {
            cerr << "Cannot open file: " << PROBE_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX << endl;
            error = true;
        }
        
        // Close the prediction file
        probeFile.close();
        
    }
    
    // Was all the data loaded successfully?
    if (!error) {
        cout << "All predictions successfully read!" << endl;
        probeLoaded = true;
    } else {
        cout << "Predictions not successfully read!" << endl;
    }
    
    return error;

}

/**
 *  @brief  Reads the prediction file for the "qual" set of each model and stores the predictions
 */
bool Blender::readQualData() {

    // Variables for reading the data
    int counter;
    bool error;
    string temp;
    ifstream predictionFile;
    
    // Variables for timing the read(s)
    clock_t start;
    double duration;
    
    error = false;
    for (int i = 0; i < NUM_MODELS && !error; i++) {
        
        // Start the clock
        start = clock();
        
        // Open the prediction file
        predictionFile.open(QUAL_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX);
        cout << "Reading prediction file " << (i + 1) << " of " << NUM_MODELS << "..." << endl;

        // Read the data from the prediction file
        if (predictionFile.is_open()) {
            
            // Loop through the file, line by line
            for (counter = 0; counter < NUM_QUAL && getline(predictionFile, temp); counter++) {
                predictions[i][counter] = atof(temp.c_str());
            }
            
            // Did the number of lines match the expected number of predictions?
            if (counter != NUM_QUAL) {
                error = true;
                cerr << "Incorrect number of predictions!" << endl;
            } else {
                // If loading was successful, print the elapsed time in seconds
                duration = (clock() - start) / (double)CLOCKS_PER_SEC;
                cout << "Data loaded. Time: " << duration << " seconds" << endl;
            }
            
        } else {
            cerr << "Cannot open file: " << QUAL_PREFIX + to_string(i + 1) + "." + FILE_SUFFIX << endl;
            error = true;
        }
        
        // Close the prediction file
        predictionFile.close();
        
    }
    
    // Was all the data loaded successfully?
    if (!error) {
        cout << "All predictions successfully read!" << endl;
        qualLoaded = true;
    } else {
        cout << "Predictions not successfully read!" << endl;
    }
    
    return error;

}

/**
 *  @brief  Prints the loaded 'probe' data. (Will probably take a very long time.)
 */
void Blender::printProbeData() {
    if (probeLoaded) {
        for (int i = 0; i < NUM_MODELS; i++) {
            cout << "[MODEL_NUMBER - PROBE_NUMBER]: [PREDICTED_VALUE]\t[ACTUAL_VALUE]" << endl;
            for (int j = 0; j < NUM_PROBE; j++) {
                cout << (i + 1) << "-" << (j + 1) << ": " << probeGuess[i][j] << "\t" << probeReal[j] <<  endl;
            }
        }
    } else {
        cerr << "Unable to print 'probe' data; not yet loaded!" << endl;
    }
}

/**
 *  @brief  Prints the loaded prediction data. (Will probably take a very long time.)
 */
void Blender::printQualData() {
    if (qualLoaded) {
        for (int i = 0; i < NUM_MODELS; i++) {
            cout << "[MODEL_NUMBER - PREDICTION_NUMBER]: [PREDICTION_VALUE]" << endl;
            for (int j = 0; j < NUM_QUAL; j++) {
                cout << (i + 1) << "-" << (j + 1) << ": " << predictions[i][j] << endl;
            }
        }
    } else {
        cerr << "Unable to print predictions; data not yet loaded!" << endl;
    }
}

/**
 *  @brief  Set the blending weights by hand (for debugging)
 */
void Blender::manualBlend() {

    // Everyone gets the same weight!
    float evenWeight = 1.0 / (float)NUM_MODELS;
    for (int i = 0; i < NUM_MODELS; i++) {
        weights[i] = evenWeight;
    }

    /*    
    // Manually set the weights for three prediction models
    weights[0] = 0.25;
    weights[1] = 0.50;
    weights[2] = 0.25;
    */

    manualWeightsDefined = true;

}

/**
 *  @brief  Saves the final, blended predictions to the specified file
 */
void Blender::writeBlendedPredictions() {

    float finalRating;

    // Display a status message
    cout << "Computing and writing final predictions..." << endl;

    // If the weights have not yet been initialized, terminate the function
    if (!finalWeightsDefined) {
        cerr << "Cannot compute final ratings! Weights not yet initialized!" << endl;
        return;
    }

    // Declare the output file
    ofstream outputFile;
  
    // Open the output file
    outputFile.open(OUTPUT_FILE);
  
    // Compute each blended rating and write to the output file
    for (int i = 0; i < NUM_QUAL; i++) {
        finalRating = w(0, 0);
        for (int j = 0; j < NUM_MODELS; j++) {
            finalRating += w(j + 1, 0) * predictions[j][i];
        }
        outputFile << finalRating << endl;   
    }
    
    // Close the output file
    outputFile.close();

}

/**
 *  @brief  Saves any manually-blended predictions to the specified file
 */
void Blender::writeManualPredictions() {

    // Declare the output file
    ofstream outputFile;

    // Variables for timing
    clock_t start;
    double duration;

    // Display a status message
    cout << "Computing and writing final predictions...";
    
    // Start the clock
    start = clock();
  
    // Open the output file
    outputFile.open(OUTPUT_FILE);
  
    // Compute each blended rating and write to the output file
    for (int i = 0; i < NUM_QUAL; i++) {
        outputFile << calcManualRating(i) << endl;   
    }
    
    // Close the output file
    outputFile.close();
    
    // Display a status message and the elapsed time in seconds
    duration = (clock() - start) / (double)CLOCKS_PER_SEC;
    cout << "complete." << endl << "Time: " << duration << " seconds." << endl;

}

/**
 *  @brief  Computes and returns the predicted rating for the given index
 */
float Blender::calcManualRating(int i) {

    float rating = 0.0;

    if (!manualWeightsDefined) {
        cerr << "Cannot compute rating! Weights not yet initialized!" << endl;
    } else {
        if ((i < 0) || (i >= NUM_QUAL)) {
            cerr << "Cannot compute rating for invalid index: " << i << endl;
        } else {
            for (int j = 0; j < NUM_MODELS; j++) {
                rating += weights[j] * predictions[j][i];
            }
        }
    }
    
    return rating;

}

/**
 *  @brief  'main' function. Creates a single Blender and runs it on the data.
 */
int main(void) {

    // Print a friendly message
    cout << "Let's blend!" << endl;

    // Create the Blender object
    Blender *blender = new Blender();

    // Read in the 'probe' data
    blender->readProbeData();
    
    // DEBUG: Print out the 'probe' data
    //blender->printProbeData();

    // Calculate the blending weights
    blender->calcWeights();

    // Read in the 'qual' data
    blender->readQualData();
    
    // Save the blending results
    blender->writeBlendedPredictions();
    
    // DEBUG: Perform a manual blend
    //blender->manualBlend();
    
    // Save the manually blended results
    //blender->writeManualPredictions();
    
    // Delete the Blender when finished
    delete blender;

    return 0;   

}
